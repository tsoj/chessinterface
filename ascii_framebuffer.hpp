#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cassert>
#include <chrono>
#include <thread>
#include <stddef.h>

#include <sys/ioctl.h>
#include <unistd.h>

namespace ascii_framebuffer
{
  struct Color
  {
    unsigned char r;
    unsigned char g;
    unsigned char b;
    friend bool operator==(const Color& a, const Color& b)
    {
      return a.r==b.r && a.g==b.g && a.b==b.b;
    }
    friend bool operator!=(const Color& a, const Color& b)
    {
      return !(a==b);
    }
  };
  constexpr Color transparent_color = {0,250,250};
  constexpr char transparent_ascii = '\r';

  class Framebuffer
  {
  private:

    size_t m_width;
    size_t m_height;
    std::vector<char> m_char_read_buffer;
    std::vector<char> m_char_write_buffer;
    std::vector<Color> m_text_color_read_buffer;
    std::vector<Color> m_text_color_write_buffer;
    std::vector<Color> m_background_color_read_buffer;
    std::vector<Color> m_background_color_write_buffer;
    std::thread m_printer_thread;
    static constexpr int frametime = 16;

    size_t get_index(const size_t col, const size_t row) const
    {
      assert(row < m_height && col < m_width && row >= 0 && col >= 0);
      return row*m_width + col;
    }

    void swap_buffers()
    {
      m_char_read_buffer = m_char_write_buffer;
      m_text_color_read_buffer = m_text_color_write_buffer;
      m_background_color_read_buffer = m_background_color_write_buffer;
    }

    template<typename F, typename T>
    void for_box(const size_t col, const size_t row, F set_function, const T& box)
    {
      for(size_t row_offset = 0; row_offset < box.size(); ++row_offset)
      {
        for(size_t col_offset = 0; col_offset < box[row_offset].size(); col_offset++)
        {
          set_function(col + col_offset, row + row_offset, box[row_offset][col_offset]);
        }
      }
    }

  public:

    Framebuffer()
    {
      winsize w;
      ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
      m_width = w.ws_col;
      m_height = w.ws_row;
      clear();
    }
    ~Framebuffer()
    {
      if(m_printer_thread.joinable())
      {
        m_printer_thread.join();
      }
    }

    size_t width() const
    {
      return m_width;
    }
    size_t height() const
    {
      return m_height;
    }

    void clear(const Color& background_color, const Color& text_color, const char c)
    {
      if(m_printer_thread.joinable())
      {
        m_printer_thread.join();
      }
      m_char_read_buffer = std::vector<char>(m_height * m_width, c);
      m_char_write_buffer = std::vector<char>(m_height * m_width, c);
      m_text_color_read_buffer = std::vector<Color>(m_height * m_width, text_color);
      m_text_color_write_buffer = std::vector<Color>(m_height * m_width, text_color);
      m_background_color_read_buffer = std::vector<Color>(m_height * m_width, background_color);
      m_background_color_write_buffer = std::vector<Color>(m_height * m_width, background_color);
    }
    void clear()
    {
      clear({0u, 0u, 0u}, {255u, 255u, 255u}, ' ');
    }

    void set_char(const size_t col, const size_t row, const char c)
    {
      if(c != transparent_ascii)
      {
        m_char_write_buffer.at(get_index(col, row)) = c;
      }
    }

    void set_char(const size_t col, const size_t row, const std::vector<std::string>& box)
    {
      for_box(col, row, [&](auto... a){set_char(a...);}, box);
    }

    void set_text_color(const size_t col, const size_t row, const Color& color)
    {
      if(color != transparent_color)
      {
        m_text_color_write_buffer.at(get_index(col, row)) = color;
      }
    }

    void set_text_color(const size_t col, const size_t row, const std::vector<std::vector<Color>>& box)
    {
      for_box(col, row, [&](auto... a){set_text_color(a...);}, box);
    }

    void set_background_color(const size_t col, const size_t row, const Color& color)
    {
      if(color != transparent_color)
      {
        m_background_color_write_buffer.at(get_index(col, row)) = color;
      }
    }

    void set_background_color(const size_t col, const size_t row, const std::vector<std::vector<Color>>& box)
    {
      for_box(col, row, [&](auto... a){set_background_color(a...);}, box);
    }

    char get_char(const size_t col, const size_t row) const
    {
      return m_char_read_buffer.at(get_index(col, row));
    }

    Color get_text_color(const size_t col, const size_t row) const
    {
      return m_text_color_read_buffer.at(get_index(col, row));
    }

    Color get_background_color(const size_t col, const size_t row) const
    {
      return m_background_color_read_buffer.at(get_index(col, row));
    }

    void print()
    {
      const auto printer = [this]()
      {
        std::stringstream output;
        output << "\033[0;0H"; // move cursor to the top left corner
        for(size_t row = 0; row < m_height; row++)
        {
          for(size_t col = 0; col < m_width; col++)
          {
            const Color text_color = get_text_color(col, row);
            const Color background_color = get_background_color(col, row);
            output
              << "\033[38;2;" // ESC[ 38;2;⟨r⟩;⟨g⟩;⟨b⟩ select RGB foreground color
              << static_cast<int>(text_color.r) << ";"
              << static_cast<int>(text_color.g) << ";"
              << static_cast<int>(text_color.b) << "m"
              << "\033[48;2;" // ESC[ 48;2;⟨r⟩;⟨g⟩;⟨b⟩ select RGB background color
              << static_cast<int>(background_color.r) << ";"
              << static_cast<int>(background_color.g) << ";"
              << static_cast<int>(background_color.b) << "m"
              << get_char(col, row);
          }
        }
        std::cout << output.rdbuf() << std::flush;
        std::this_thread::sleep_for(std::chrono::milliseconds(frametime));
      };

      if(m_printer_thread.joinable())
      {
        m_printer_thread.join();
      }
      swap_buffers();
      m_printer_thread = std::thread(printer);
    }
  };
}
