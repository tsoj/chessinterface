#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cassert>
#include <chrono>
#include <thread>
#include <stddef.h>

#include <sys/ioctl.h>
#include <unistd.h>

struct Color
{
  unsigned char r;
  unsigned char g;
  unsigned char b;
};

class Framebuffer
{
public:

  const size_t width;
  const size_t height;

  Framebuffer() :
    width([](){
      winsize w;
      ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
      return w.ws_col;
    }()),
    height([](){
      winsize w;
      ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
      return w.ws_row;
    }())
  {
    charBuffer = std::vector<char>(height * width, ' ');
    textColorBuffer = std::vector<Color>(height * width, {255u, 255u, 255u});
    backgroundColorBuffer = std::vector<Color>(height * width, {0u, 0u, 0u});
  }
  ~Framebuffer()
  {
    if(printerThread.joinable())
    {
      printerThread.join();
    }
  }

  void clear()
  {
    charBuffer.assign(charBuffer.size(), ' ');
    textColorBuffer.assign(textColorBuffer.size(), {255u, 255u, 255u});
    backgroundColorBuffer.assign(backgroundColorBuffer.size(), {0u, 0u, 0u});
  }

  void setChar(size_t col,size_t row, char c)
  {
    assert(row < height && col < width && row >= 0 && col >= 0);
    charBuffer.at(row*width + col) = c;
  }

  void setChar(size_t col, size_t row, const std::vector<std::string>& box)
  {
    assert(row < height && col < width && row >= 0 && col >= 0);
    for(size_t rowOffset = 0; rowOffset < box.size(); rowOffset++)
    {
      for(size_t colOffset = 0; colOffset < box[rowOffset].size(); colOffset++)
      {
        setChar(col + colOffset, row + rowOffset, box[rowOffset][colOffset]);
      }
    }
  }

  void setTextColor(size_t col, size_t row, Color color)
  {
    assert(row < height && col < width && row >= 0 && col >= 0);
    textColorBuffer.at(row*width + col) = color;
  }

  void setTextColor(size_t col, size_t row, const std::vector<std::vector<Color>>& box)
  {
    assert(row < height && col < width && row >= 0 && col >= 0);
    for(size_t rowOffset = 0; rowOffset < box.size(); rowOffset++)
    {
      for(size_t colOffset = 0; colOffset < box[rowOffset].size(); colOffset++)
      {
        setTextColor(col + colOffset, row + rowOffset, box[rowOffset][colOffset]);
      }
    }
  }

  void setBackgroundColor(size_t col,size_t row, Color color)
  {
    assert(row < height && col < width && row >= 0 && col >= 0);
    backgroundColorBuffer.at(row*width + col) = color;
  }

  void setBackgroundColor(size_t col, size_t row, const std::vector<std::vector<Color>>& box)
  {
    assert(row < height && col < width && row >= 0 && col >= 0);
    for(size_t rowOffset = 0; rowOffset<box.size(); rowOffset++)
    {
      for(size_t colOffset = 0; colOffset < box[rowOffset].size(); colOffset++)
      {
        setBackgroundColor(col + colOffset, row + rowOffset, box[rowOffset][colOffset]);
      }
    }
  }

  char getChar(size_t col, size_t row) const
  {
    assert(row < height && col < width && row >= 0 && col >= 0);
    return charBuffer.at(row*width + col);
  }

  Color getTextColor(size_t col, size_t row) const
  {
    assert(row < height && col < width && row >= 0 && col >= 0);
    return textColorBuffer.at(row*width + col);
  }

  Color getBackgroundColor(size_t col, size_t row) const
  {
    assert(row < height && col < width && row >= 0 && col >= 0);
    return backgroundColorBuffer.at(row*width + col);
  }

  void print() const
  {
    if(printerThread.joinable())
    {
      printerThread.join();
    }
    static const auto printer = [this]()
    {
      std::stringstream output;
      for(size_t row = 0; row < height; row++)
      {
        for(size_t col = 0; col < width; col++)
        {
          const Color textColor = getTextColor(col, row);
          const Color backgroundColor = getBackgroundColor(col, row);
          output
            << "\033[38;2;"
            << static_cast<int>(textColor.r) << ";"
            << static_cast<int>(textColor.g) << ";"
            << static_cast<int>(textColor.b) << "m"
            << "\033[48;2;"
            << static_cast<int>(backgroundColor.r) << ";"
            << static_cast<int>(backgroundColor.g) << ";"
            << static_cast<int>(backgroundColor.b) << "m"
            << getChar(col, row);
        }
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(frametime));
      std::system("clear");
      std::cout << output.rdbuf() << std::flush;
    };
    printerThread = std::thread(printer);
  }

private:

    std::vector<char> charBuffer;
    std::vector<Color> textColorBuffer;
    std::vector<Color> backgroundColorBuffer;
    static const int frametime = 33;
    inline static std::thread printerThread;
};
