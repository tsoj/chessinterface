#include "ascii_framebuffer.hpp"
#include "Googleplex_Starthinker/src/Position.hpp"
#include "Googleplex_Starthinker/src/Search.hpp"
#include "Googleplex_Starthinker/src/Uci.hpp"

#include <cstring>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <iostream>
#include <fstream>

#include <termios.h>

#define RAII_LOCK(mutexname) std::lock_guard<std::mutex> guard(mutexname);

char getch()
{
  char buf = 0;
  termios attr;
  std::memset(&attr, 0, sizeof(termios));
  if (tcgetattr(0, &attr) < 0)
  {
    perror("tcsetattr()");
    assert(false);
  }
  termios backup = attr;
  attr.c_lflag &= ~ICANON;
  attr.c_lflag &= ~ECHO;
  attr.c_cc[VMIN] = 1;
  attr.c_cc[VTIME] = 0;
  if (tcsetattr(0, TCSANOW, &attr) < 0)
  {
    perror("tcsetattr ICANON");
    assert(false);
  }
  if (read(0, &buf, 1) < 0)
  {
    perror ("read()");
    assert(false);
  }
  if (tcsetattr(0, TCSADRAIN, &backup) < 0)
  {
    perror ("tcsetattr ~ICANON");
    assert(false);
  }
  return (buf);
}


const std::string CHESS_ENGINE_LOGO =
std::string("     ___________   ___________\n") +
std::string("    /  _____   /   \\   _______\\      /\\\n") +
std::string("   /  /    /__/     \\  \\_______    <    >\n") +
std::string("  /  /    ___        \\_______  \\     \\/\n") +
std::string(" /  /____/  /         _______\\  \\\n") +
std::string("/_______   /oogleplex \\__________\\tarthinker\n") +
std::string("       /  /\n") +
std::string("   ___/  /\n") +
std::string("  /_____/");

constexpr size_t DEFAULT_HASH_SIZE = 256;
constexpr size_t MB = 1'000'000;

const std::string STARTPOS_FEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

using namespace ascii_framebuffer;

const std::vector<std::string> pieceSymbols[7] =
{
  // PAWN
  {
    "         ",
    "    o    ",
    "   ( )   ",
    "   |_|   "
  },
  // KNIGHT
  {
    "    __,  ",
    "  /  o\\  ",
    "  \\  \\_> ",
    "  /__\\   "
  },
  // BISHOP
  {
    "    o    ",
    "   ( )   ",
    "   / \\   ",
    "  /___\\  "
  },
  // ROOK
  {
    "  n_n_n  ",
    "  \\   /  ",
    "  |   |  ",
    "  /___\\  "
  },
  // QUEEN
  {
    "  ooooo  ",
    "   \\ /   ",
    "   / \\   ",
    "  /___\\  "
  },
  /*// QUEEN
  {
    "  ooooo  ",
    "   \\ /   ",
    "   ( )   ",
    "   /_\\   "
  },*/
  // KING
  {
    "    +    ",
    "   \\ /   ",
    "   ( )   ",
    "   /_\\   "
  },
  // NO_PIECE
  {
    "         ",
    "         ",
    "         ",
    "         "
  }
};

const Color darkSquareColor = {200,0,0};//{120,120,120};
const Color lightSquareColor = {150,150,150};//{150,150,150};
const Color pieceColor[3] = {{255,255,255},{0,0,0},{0,0,0}};
const Color commandlineColor = {50,50,50};

const size_t squareWidth = 9;
const size_t squareHeight = 4;

void drawPosition(Framebuffer& framebuffer, const Position& position, const Player& facingBoardSide)
{
  const size_t offsetCol = 0;
  const size_t offsetRow = 1;

  std::vector<std::vector<Color>> backgroundColorBox = std::vector<std::vector<Color>>(8*squareHeight, std::vector<Color>(8*squareWidth));
  for(size_t y = 0; y<8; y++)
  {
    for(size_t x = 0; x<8; x++)
    {
      Color currentBackgroundColor = darkSquareColor;
      if(y%2==0 && x%2==0)
      {
        currentBackgroundColor = lightSquareColor;
      }
      if(y%2==1 && x%2==1)
      {
        currentBackgroundColor = lightSquareColor;
      }
      for(size_t h = 0; h<squareHeight; h++)
      {
        for(size_t w = 0; w<squareWidth; w++)
        {
          backgroundColorBox[y*squareHeight + h][x*squareWidth + w] = currentBackgroundColor;
        }
      }
    }
  }
  framebuffer.set_background_color(offsetCol, offsetRow, backgroundColorBox);

  for(size_t rank = 0; rank<8; rank++)
  {
    for(size_t file = 0; file<8; file++)
    {
      Square square = facingBoardSide == WHITE ? rank*8+file : Util::rotateCounterclockwise(Util::rotateCounterclockwise(rank*8+file));
      auto pieceType = position.getPiece(square);
      std::vector<std::vector<Color>> currentPieceColorBox =
        std::vector<std::vector<Color>>(squareHeight, std::vector<Color>(squareWidth, pieceColor[pieceType.player]));
      std::vector<std::string> currentPieceSymbolBox = pieceSymbols[pieceType.piece];

      framebuffer.set_text_color(file*squareWidth + offsetCol, (7-rank)*squareHeight + offsetRow, currentPieceColorBox);
      framebuffer.set_char(file*squareWidth + offsetCol, (7-rank)*squareHeight + offsetRow, currentPieceSymbolBox);
    }
  }

  const std::vector<std::string> whosToMoveString[3] = {{"White to move"}, {"Black to move"}, {"Nobody to move"}};
  framebuffer.set_char(0 + offsetCol, 8*squareHeight + offsetRow, whosToMoveString[position.us]);

}
void drawCommandline(Framebuffer& framebuffer, std::string s)
{
  size_t commandlineWidth = framebuffer.width() - 2 - 8*squareWidth;
  size_t commandlineHeight = framebuffer.height() - 2;
  size_t row = 1;
  size_t col = 8*squareWidth + 1;
  assert(commandlineWidth >= 0);

  std::vector<std::vector<Color>> backgroundColorBox =
    std::vector<std::vector<Color>>(commandlineHeight, std::vector<Color>(commandlineWidth, commandlineColor));
  framebuffer.set_background_color(col, row, backgroundColorBox);

  std::vector<std::string> lines;
  size_t i = 0;
  for(size_t line = 0; line<commandlineHeight; line++)
  {
    lines.push_back(std::string(commandlineWidth, ' '));
    for(size_t j = 0; j<commandlineWidth; j++)
    {
      if(s.size() > i)
      {
        if(s[i] != '\n')
        {
          lines.back()[j] = s[i];
        }
        else
        {
          j=commandlineWidth;
        }
        i++;
      }
    }
  }
  framebuffer.set_char(col, row, lines);
}
void drawClock(Framebuffer& framebuffer, int wtime, int btime)
{
  int wtimeMin = wtime/60000;
  int wtimeSec = (wtime%60000)/1000;
  int btimeMin = btime/60000;
  int btimeSec = (btime%60000)/1000;

  std::string wtimeStr = std::to_string(wtimeMin) + ":" + std::to_string(wtimeSec);
  std::string btimeStr = std::to_string(btimeMin) + ":" + std::to_string(btimeSec);

  for(size_t i = 0; i<72; i++)
  {
    framebuffer.set_char(i, 0, ' ');
  }
  framebuffer.set_char(18, 0, {"White: " + wtimeStr});
  framebuffer.set_char(45, 0, {"Black: " + btimeStr});
}

class Application
{
public:

  Application()
  {
    std::ifstream skillLevelFile;
    skillLevelFile.open("skillLevel.txt");
    if(skillLevelFile.peek() == std::ifstream::traits_type::eof())
    {
      skillLevel = 99;
    }
    else
    {
      std::string line;
      std::getline(skillLevelFile, line);
      skillLevel = std::clamp(std::stoi(line), 0, 99);
    }
    skillLevelFile.close();
  }

  ~Application()
  {
    std::ofstream skillLevelFile;
    skillLevelFile.open("skillLevel.txt");
    skillLevelFile << std::to_string(skillLevel) << "\n";
    skillLevelFile.close();

    std::ofstream logFile;
    logFile.open("log.txt", std::ios_base::app);
    logFile << log << "\nLast position: " << position.getFen();
    logFile.close();

    if(engineThread.joinable())
    {
      engineThread.join();
    }
  }

  void run()
  {
    running = true;
    newGame();
    std::thread clockThread = std::thread(&Application::clickClock, std::ref(*this));
    std::thread computerIsThinkingThread = std::thread(&Application::computerIsThinking, std::ref(*this));
    while(running)
    {
      draw();

      char input = getch();
      receivedInput(input);
    }
    clockThread.join();
    computerIsThinkingThread.join();
  }

private:

  Framebuffer framebuffer;
  std::atomic<bool> running;
  std::atomic<bool> clockTicking = false;

  std::atomic<bool> engineNotRunning = true;
  TranspositionTable tt = TranspositionTable(DEFAULT_HASH_SIZE*MB);
  int skillLevel = 99;
  const Value randomDelta = 5;
  std::vector<Position> positionHistory;
  std::vector<Move> moveHistory;
  std::thread engineThread;
  std::atomic<bool> autoengine = false;

  int winc;
  int binc;
  int wtime ;
  int btime;

  Position position;
  Position visiblePosition;

  Player facingBoardSide;

  std::string inputBuffer;
  std::string textBuffer = CHESS_ENGINE_LOGO;
  std::string log = "";
  std::mutex appLock;

  struct Input{ char c; };
  struct Command{ std::string s; };

  void draw()
  {
    if(running)
    {
      RAII_LOCK(appLock);

      drawPosition(framebuffer, visiblePosition, facingBoardSide);
      drawCommandline(framebuffer, textBuffer);
      drawClock(framebuffer, wtime, btime);

      framebuffer.print();
    }
  }
  void clickClock()
  {
    std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
    std::chrono::time_point<std::chrono::steady_clock> lastClick = now;
    while(running)
    {
      now = std::chrono::steady_clock::now();
      if(clockTicking)
      {
        appLock.lock();
        if(position.us == WHITE)
        {
          wtime -= std::chrono::duration_cast<std::chrono::milliseconds>(now-lastClick).count();
        }

        if(position.us == BLACK)
        {
          btime -= std::chrono::duration_cast<std::chrono::milliseconds>(now-lastClick).count();
        }
        appLock.unlock();
        draw();
      }
      lastClick = now;
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
  }
  void computerIsThinking()
  {
    int counter = 0;
    char spinner[8] = {'-','\\','|','/','-','\\','|','/'};
    while(running)
    {
      if(!engineNotRunning)
      {
        appLock.lock();

        if(textBuffer == "" || textBuffer == (std::string("Computer is thinking ") + spinner[((counter+8)-1)%8]))
        {
          textBuffer = std::string("Computer is thinking ") + spinner[counter];
        }

        counter++;
        if(counter >= 8)
        {
          counter = 0;
        }
        appLock.unlock();

        draw();
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
  }

  void setPosition(const Position& p)
  {
    RAII_LOCK(appLock);
    position = p;
    visiblePosition = position;
    positionHistory.clear();
    positionHistory.push_back(position);
    moveHistory.clear();
    log += "\n\n" + position.getFen() + "\n";
  }
  bool setPosition(const Move& m)
  {
    RAII_LOCK(appLock);
    if(Util::isLegalMove(m, position))
    {
      if(clockTicking)
      {
        if(position.us == WHITE)
        {
          wtime += winc;
        }
        else
        {
          btime += binc;
        }
      }

      log += m.getNotation() + " ";
      position.doMove(m);
      visiblePosition = position;
      positionHistory.push_back(position);
      moveHistory.push_back(m);
    }
    else
    {
      textBuffer = "Illegal move: " + m.getNotation() + ". Legal moves:\n";
      Move moves[MAX_NUM_QUIETS+MAX_NUM_CAPTURES];
      uint8_t numMoves = position.generateMoves(moves);
      for(uint8_t i = 0; i<numMoves; i++)
      {
        if(Util::isLegalMove(moves[i], position))
        {
          textBuffer += moves[i].getNotation();
          if(i != numMoves-1)
          {
            textBuffer += ", ";
          }
        }
      }
      return false;
    }
    return true;
  }
  void undoPosition()
  {
    RAII_LOCK(appLock);
    if(positionHistory.size()>1)
    {
      log += "undo:" + moveHistory.back().getNotation() + " ";
      positionHistory.pop_back();
      moveHistory.pop_back();
      position = positionHistory.back();
      visiblePosition = position;
      tt.clear();
    }
  }

  void newGame()
  {
    engineNotRunning = true;
    setPosition(Position::fen(STARTPOS_FEN));
    RAII_LOCK(appLock);
    winc = 0;
    binc = 0;
    wtime = 3600000;
    btime = 3600000;
    clockTicking = false;
    facingBoardSide = WHITE;
    textBuffer = CHESS_ENGINE_LOGO;
    autoengine = false;
    tt.clear();
  }

  void updateSkill(int s)
  {
    RAII_LOCK(appLock);
    skillLevel = std::clamp(skillLevel+s, 0, 99);
  }

  void lostGame()
  {
    appLock.lock();
    textBuffer = "You lost";
    appLock.unlock();
    draw();
    updateSkill(-1);
  }
  void wonGame()
  {
    appLock.lock();
    textBuffer = "You won";
    appLock.unlock();
    draw();
    updateSkill(1);
  }
  void drawGame()
  {
    appLock.lock();
    textBuffer = "You draw";
    appLock.unlock();
    draw();
  }

  void doEngineMove()
  {
    // TODO: do random book move
    if(engineThread.joinable())
    {
      engineThread.join();
    }

    engineThread = std::thread([this]()
    {
      int msLeft = position.us == WHITE ? wtime : btime;
      int msIncPerMove = position.us == WHITE ? winc : binc;
      engineNotRunning = false;
      Move bestMove = Search::go(
        position,
        tt,
        positionHistory,
        MAX_DEPTH,
        msLeft,
        msIncPerMove,
        -1,
        engineNotRunning,
        randomDelta,
        skillLevel
      );

      engineNotRunning = true;

      setPosition(bestMove);

      appLock.lock();
      textBuffer = "Computer played " + bestMove.getNotation();

      auto gameResult = Util::gameEnded(position, positionHistory);
      appLock.unlock();

      if(gameResult == DRAW)
      {
        drawGame();
      }
      else if(gameResult == CHECKMATE)
      {
        lostGame();
      }

      draw();
    }
    );
  }

  void receivedInput(char input)
  {
    if(input == 127 || input == 8) // backspace key
    {
      RAII_LOCK(appLock);
      if(inputBuffer.size() > 0)
      {
        inputBuffer.pop_back();
      }
    }
    else if(input == '\n')
    {
      appLock.lock();
      textBuffer.clear();
      appLock.unlock();

      receivedCommand(inputBuffer);

      RAII_LOCK(appLock);
      inputBuffer.clear();
    }
    else
    {
      RAII_LOCK(appLock);
      inputBuffer += input;
    }
    if(input != '\n')
    {
      RAII_LOCK(appLock);
      textBuffer = inputBuffer + "_";
    }
  }

  void receivedCommand(const std::string& command)
  {
    std::vector<std::string> params = Util::split(command, ' ');
    if(params.empty())
    {
      return;
    }
    if(params[0] == "skill")
    {
      if(params.size() >= 2)
      {
        RAII_LOCK(appLock);
        skillLevel = std::clamp(std::stoi(params[1]), 0, 99);
      }
      else
      {
        RAII_LOCK(appLock);
        textBuffer = "skill level: " + std::to_string(skillLevel);
      }
    }
    else if(params[0] == "quit")
    {
      engineNotRunning = true;
      running = false;
    }
    else if(params[0] == "fen" && engineNotRunning)
    {
      if(params.size() >= 7)
      {
        std::string fen = params[1] + " " + params[2] + " " + params[3] + " " + params[4] + " " + params[5] + " " + params[6];
        setPosition(Position::fen(fen));
      }
      else
      {
        RAII_LOCK(appLock);
        textBuffer = "FEN not formatted correctly";
      }
    }
    else if(params[0] == "getfen")
    {
      RAII_LOCK(appLock);
      textBuffer = position.getFen();
    }
    else if(params[0] == "swap")
    {
      RAII_LOCK(appLock);
      facingBoardSide = switchPlayer(facingBoardSide);
    }
    else if(params[0] == "newgame" && engineNotRunning)
    {
      newGame();
    }
    else if(params[0] == "resign")
    {
      lostGame();
    }
    else if(params[0] == "undo" && engineNotRunning)
    {
      undoPosition();
    }
    else if(params[0] == "logo")
    {
      RAII_LOCK(appLock);
      textBuffer = CHESS_ENGINE_LOGO;
    }
    else if(params[0] == "stop")
    {
      engineNotRunning = true;
    }
    else if(params[0] == "go" && engineNotRunning)
    {
      doEngineMove();
    }
    else if(params[0] == "autoengine" && engineNotRunning)
    {
      if(params.size() > 1)
      {
        if(params[1] == "on")
        {
          autoengine = true;
        }
        else if(params[1] == "off")
        {
          autoengine = false;
        }
      }
    }
    else if(params[0] == "tc")
    {
      RAII_LOCK(appLock);
      if(params.size() > 4)
      {
        if(params[1].back() == 's')
        {
          params[1].pop_back();
          wtime = std::stoi(params[1])*1000;
        }
        else if(params[1].back() == 'm')
        {
          params[1].pop_back();
          wtime = std::stoi(params[1])*1000*60;
        }
        else if(params[1].back() == 'h')
        {
          params[1].pop_back();
          wtime = std::stoi(params[1])*1000*60*60;
        }
        else
        {
          // default is minute
          wtime = std::stoi(params[1])*1000*60;
        }

        if(params[2].back() == 's')
        {
          params[2].pop_back();
          winc = std::stoi(params[2])*1000;
        }
        else if(params[2].back() == 'm')
        {
          params[2].pop_back();
          winc = std::stoi(params[2])*1000*60;
        }
        else if(params[2].back() == 'h')
        {
          params[2].pop_back();
          winc = std::stoi(params[2])*1000*60*60;
        }
        else
        {
          // default is second
          winc = std::stoi(params[2])*1000;
        }


        if(params[3].back() == 's')
        {
          params[3].pop_back();
          btime = std::stoi(params[3])*1000;
        }
        else if(params[3].back() == 'm')
        {
          params[3].pop_back();
          btime = std::stoi(params[3])*1000*60;
        }
        else if(params[3].back() == 'h')
        {
          params[3].pop_back();
          btime = std::stoi(params[3])*1000*60*60;
        }
        else
        {
          // default is minute
          btime = std::stoi(params[3])*1000*60;
        }

        if(params[4].back() == 's')
        {
          params[4].pop_back();
          binc = std::stoi(params[4])*1000;
        }
        else if(params[4].back() == 'm')
        {
          params[4].pop_back();
          binc = std::stoi(params[4])*1000*60;
        }
        else if(params[4].back() == 'h')
        {
          params[4].pop_back();
          binc = std::stoi(params[4])*1000*60*60;
        }
        else
        {
          // default is second
          binc = std::stoi(params[4])*1000;
        }
      }
    }
    else if(params[0] == "startclock")
    {
      clockTicking = true;
    }
    else if(params[0] == "pauseclock")
    {
      clockTicking = false;
    }
    else if(params[0] == "help")
    {
      RAII_LOCK(appLock);
      textBuffer =  std::string("quit - exits the program\n\n") +
                    std::string("newgame - starts new game\n\n") +
                    std::string("resign - resign\n\n") +
                    std::string("fen [fenstring] - sets the board from a FEN string\n\n") +
                    std::string("getfen - prints out current position as FEN string\n\n") +
                    std::string("swap - swaps the board\n\n") +
                    std::string("autoengine [on,off] - enables/disables instant engine start on move\n\n") +
                    std::string("go - engine makes a move\n\n") +
                    std::string("[move] - play this move\n    E.g.: e2e4, e7e5, e1g1 (white short castling), e7e8q (promotion)\n\n") +
                    std::string("undo - withdraws last move\n\n") +
                    std::string("stop - stops engine\n\n") +
                    std::string("skill [level] - shows engine skill level; when second argument provided sets skill level\n\n") +
                    std::string("tc [white time] [white inc] [black time] [black inc] - sets time control (1h = 60m = 3600s)\n\n") +
                    std::string("startclock- starts the clock\n\n") +
                    std::string("pauseclock- pauses the clock\n\n") +
                    std::string("logo - shows logo\n\n") +
                    std::string("help - shows this dialog");
    }
    else if(Util::isMove(params[0]) && engineNotRunning)
    {
      if(setPosition(Util::getMove(params[0], position)))
      {
        auto gameResult = Util::gameEnded(position, positionHistory);
        if(gameResult == CHECKMATE)
        {
          wonGame();
        }
        else if(gameResult == DRAW)
        {
          drawGame();
        }
        else if(autoengine)
        {
          doEngineMove();
        }
      }
    }
    else
    {
      RAII_LOCK(appLock);
      textBuffer = "Unkown command: " + params[0];
    }
  }
};


int main()
{
  Application app;
  app.run();
  return 0;
}
