CC	= clang++
CFLAGS  = -std=c++17 -O3 -Wall -Wextra -Wpedantic -fno-omit-frame-pointer -g #-fsanitize=undefined
LDFLAGS = -std=c++17 -O3 -Wall -Wextra -Wpedantic -pthread -fno-omit-frame-pointer -g #-fsanitize=undefined
NAME = chessInterface
SRC_FILE_PATH = ./
BIN_FILE_PATH = ./bin/

CPP = main.cpp

OBJ = $(CPP:%.cpp=$(BIN_FILE_PATH)%.o)

TMP = Util.o Position.o Movegen.o Uci.o MoveSelector.o Search.o TranspositionTable.o Evalutation.o pst.o
EXT_OBJ = $(TMP:%.o=$(BIN_FILE_PATH)%.o)

-include $(OBJ:%.o=%.d)

all: $(OBJ)
	make -C ./Googleplex_Starthinker/ -f makefile
	touch ./Googleplex_Starthinker/bin/Linux/main.o
	rm ./Googleplex_Starthinker/bin/Linux/main.o
	cp ./Googleplex_Starthinker/bin/Linux/*.o ./bin/
	$(CC) -o $(BIN_FILE_PATH)$(NAME) $(OBJ) $(EXT_OBJ) $(LDFLAGS)

$(BIN_FILE_PATH)%.o: $(SRC_FILE_PATH)%.cpp
	$(CC) $(CFLAGS) -c $(SRC_FILE_PATH)$*.cpp -o $(BIN_FILE_PATH)$*.o
	echo -n $(BIN_FILE_PATH) > $(BIN_FILE_PATH)$*.d
	$(CC) $(CFLAGS) -MM $(SRC_FILE_PATH)$*.cpp >> $(BIN_FILE_PATH)$*.d

test: all
	$(BIN_FILE_PATH)$(NAME)

clean:
	rm $(BIN_FILE_PATH)*
